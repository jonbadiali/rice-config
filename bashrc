#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# Set environment variables
export GOPATH=/home/$USER/projects/go
PS1='[\u@\h \W]\$ '

# Functions
qemu() {
  if [[ "" == $2 ]]; then
      mem="4G"
  else
      mem=$2
  fi
  if [[ "" == $1 ]]; then
    echo "Please specify an img file"
  else
    qemu-system-x86_64 -drive file=$HOME/img/vm/raw/$1.img,format=raw,cache=none -enable-kvm -cpu host -nic user,hostfwd=tcp::8000-:7400 -m $mem
  fi
}

# Aliases
alias ls='ls --color=auto'
alias httpy='python -m http.server'
